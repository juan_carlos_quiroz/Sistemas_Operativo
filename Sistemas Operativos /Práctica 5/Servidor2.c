#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

int cont=0;
int lista[50];
int pos=0;
void Metodo_hilos(void* p){
	int msj,msj2,i;
	int *pid = (int*)p;
	int descTSC,descTCS;
	cont++;	
	char tubTSC[15];
	char tubTCS[15];
	sprintf(tubTSC, "tubTSC%d.tub",*pid);
	sprintf(tubTCS, "tubTCS%d.tub",*pid);

	printf("\tHilo: estableciendo conexion con %d\n", *pid);
	if(mknod(tubTCS, S_IFIFO|0666,0) == -1){
		perror("No se pudo crear la tuberia");
		exit(-1);	
	}
	printf("\tHilo: %s creada\n", tubTCS);
	if(mknod(tubTSC, S_IFIFO|0666,0) == -1){
		perror("No se pudo crear la tuberia");
		exit(-1);	
	}	
	printf("\tHilo: %s creada\n", tubTSC);
	if((descTCS=open(tubTCS, O_RDONLY)) == -1){
		perror("No se pudo abrir la tuberia");
		exit(-1);	
	}
	printf("\tHilo: %s abierta\n", tubTCS);
	if((descTSC=open(tubTSC, O_WRONLY)) == -1){
		perror("No se pudo abrir la tuberia");
		exit(-1);	
	}
	printf("\tHilo: %s abierta\n", tubTSC);

	printf("\t\tHilo: esperando solicitud de %d\n",*pid);
	read(descTCS,&msj,sizeof(int));
	
	while(msj==2){
		printf("\t\tHilo: enviando lista a %d\n",*pid);
		for(i=0;i<pos;i++){
			msj2=lista[i];
			write(descTSC,&msj2,sizeof(int));
		}
		msj2=0;
		write(descTSC,&msj2,sizeof(int));
		read(descTCS,&msj,sizeof(int));
	}
	printf("\t\t\tHilo: finalizando conexion con %d\n",*pid);
	close(descTCS);
	close(descTSC);
	printf("\t\t\tHilo: proceso %d desconectado\n",*pid);
	cont--;
	pthread_exit(NULL);
}
int Busca_Lista(int pid){
	int i;
	for(i=0;i<pos;i++){
		if(lista[i]==pid){
			return 1;
		}
	}
	return 0;
}
int main(){
	int pid;
	int descCNX;
	pthread_t Hilos[50];
	if(mknod("tubCNX.tub", S_IFIFO|0666,6) == -1){
		perror("No se pudo ABRIR la tuberia tubCNX.tub");
		exit(-1);
	}
	printf("Servidor: humano, ejecutar cliente porfavor.\n");

	if((descCNX=open("tubCNX.tub", O_RDONLY)) == -1){
		perror("No se pudo ABRIR la tuberia tubCNX.tub");
		exit(-1);
	}
	printf("\nServidor: esperando solicitud de conexion... \n");
	read(descCNX,&pid,sizeof(int));
	printf("\nProcesando solicitud de: %d \n",pid);
	lista[pos]=pid;
	pthread_create(&Hilos[pos],NULL,(void*)&Metodo_hilos,(void*)&lista[pos]);
	pos++;
	sleep(2);

	while(1){
		if(cont==0){
			break;
		}
		read(descCNX,&pid,sizeof(int));
		if(Busca_Lista(pid)==0){
			printf("\nProcesando solicitud de: %d \n",pid);
			lista[pos]=pid;
			pthread_create(&Hilos[pos],NULL,(void*)&Metodo_hilos,(void*)&lista[pos]);
			pos++;
		}
	}	

		for(int i=0;i<cont;i++){
			pthread_join(Hilos[i],NULL);
		}
	

	

	close(descCNX);
	printf("Servidor: Termina la ejecucion\n");
}