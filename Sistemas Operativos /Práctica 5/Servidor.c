#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

int main(){
	int descTCX;
	int msj = 4;
	if(mknod("tubTCX.tub", S_IFIFO|0666,0) == -1){
		perror("No se pudo crear la tuberia");
		exit(-1);
	}
	printf("Servidor: Tuberia creada... \n");
	printf("Servidor: Ejecutar cliente\n");

	if((descTCX=open("tubTCX.tub",O_RDONLY)) == -1){
		perror("No se pudo abrir la tuberia");
		exit(-1);
	}
	printf("servidor: Tuberia abierta... \n");
	printf("Cliente: Leyendo de la tuberia\n");
	while(1){
		read(descTCX,&msj,sizeof(int));
		if(msj==-1){
			break;
		}
		printf("Mensaje recibido: %d\n",msj);
	}
	close(descTCX);
	printf("Servidor: termina la ejecucion\n");

	return 0;
}

