#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

int main(){
	int opcion,pid;
	int descCNX, descTSC, descTCS;
	char tubCNX[15], tubTSC[15], tubTCS[15];
	int lista [50];
	pid=getpid();
	sprintf(tubCNX,"tubCNX.tub");
	sprintf(tubTSC,"tubTSC%d.tub",pid);
	sprintf(tubTCS,"tubTCS%d.tub",pid);
	int msj2=0;
	if((descCNX=open(tubCNX,O_WRONLY))==-1){
		perror("No se pudo abrir el pipe");
		//exit(-1);
	}
	do{
		printf("1.-Solicitar conexion.\n");
		printf("2.-Solicitar lista de conectados.\n");
		printf("3.-Solicitar desconexion.\n");
		scanf("%d",&opcion);
		switch(opcion){
			case 1: printf("Enviando pid %d para conexion\n\n", pid);
				if(write(descCNX,&pid,sizeof(int))==-1){
					perror("No se pudo escribir en la tuberia");
					exit(-1);
				}
				sleep(1);
				if((descTCS=open(tubTCS,O_WRONLY))==-1){
					perror("No se pudo abrir la tuberia para escritura");
					exit(-1);
				}
				sleep(1);
				if((descTSC=open(tubTSC,O_RDONLY))==-1){
					perror("No se pudo abrir la tuberia para lectura");
					exit(-1);
				}
				break;
			case 2: printf("Solicitando la lista de conectados\n\n");
				if(write(descTCS,&opcion,sizeof(int))==-1){
					perror("No se pudo escribir en la tuberia");
					exit(-1);
				}
				if(read(descTSC,&msj2,sizeof(int))==-1){
					perror("No se pudo escribir en la tuberia");
					exit(-1);
				}
				do{
					printf("[ %d ],", msj2);
					read(descTSC,&msj2,sizeof(int));
				}while(msj2!=0);
				printf("\n\n");
				break;
			case 3: printf("Solicitando desconexion\n\n");
				if(write(descTCS,&opcion,sizeof(int))==-1){
					perror("No se pudo escribir en la tubeia");
					exit(-1);
				}
				break;
			default: printf("Opcion no valida\n\n");
		}
	}while(opcion!=3);
	close(descCNX);
	printf("Cliente Finalizado\n");
}