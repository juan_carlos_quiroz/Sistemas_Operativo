#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

int main(){
	int descTCX;
	char buf[10];
	int msj; 
	if((descTCX = open("tubTCX.tub",O_WRONLY)) == -1){
		perror("No se pudo abrir la tuberia");
		exit(-1);
	}
	printf("Cliente: Tuberia abierta... \n");
	while(1){
		printf("Ingrese un numero o -1 para salir del programa : ");
		scanf("%d", &msj);
		if(msj==-1){
			write(descTCX, &msj, sizeof(int));
			break;
		}
		printf("Servidor: Escribiendo \"mensaje\" en la tuberia... \n");
		write(descTCX, &msj, sizeof(int));
	}
	printf("Cliente: Finalizando\n");
	close(descTCX);
	exit(0);	
}

