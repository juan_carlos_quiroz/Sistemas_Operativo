#!/bin/bash

    if [ "$1" = "" ]
    then
        echo "Debe indicar una contraseña a utilizar."
        exit
    fi
    
    c=`expr length $1`
    
    if [ $c -lt 8 ]
    then
        echo "La contraseña debe tener al menos 8 caracteres"
        exit
    fi

    if  echo "$1" | grep -v [0-9] 
   	then 
    	echo "La contraseña debe tener al menos un numero"
    	exit
    fi

    if echo "$1" | egrep  "@|#|%|&|%|-|="
	then 
	    echo "la contraseña es valida"
	else
    	echo "la contraseña debe tener al menos un caracter @,#,%,&,-,= "
    	exit
    fi