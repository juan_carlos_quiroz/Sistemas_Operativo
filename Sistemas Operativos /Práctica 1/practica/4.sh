#!/bin/bash
echo "Sleep interrumpible"
ps -eo stat | grep S | wc -l

echo "Sleep ininterrumpible"
ps -eo stat | grep D | wc -l

echo "Stopped"
ps -eo stat | grep T | wc -l

echo "Running"
ps -eo stat | grep R | wc -l

echo "Zombie"
ps -eo stat | grep Z | wc -l
