#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

#define N 5

int main(){
    int i,status=0;
    int cont=0;
    for(i=0;i<N;i++){
        if(!fork()){
            break;
        }
    }
    
    sleep(8);
    
    if(i<N){
       cont=1+WEXITSTATUS(status);
       printf(" %d: conteo= %d procesos terminados\n",getpid(),cont); 
       exit(cont);
        
    }else{
        for(i=0;i<N;i++){
            wait(&status);
	    cont = cont + WEXITSTATUS(status);  
        }
        printf(" %d: conteo= %d procesos terminados\n",getpid(),cont);
    }
    
    return 1;
}
