#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
    int op;
    char *argsv[]={"/usr/bin/xterm","-fn","9x15",NULL};
    char *argsvp[]={"/usr/bin/xterm","-fn","9x15",NULL};
    
    printf("seleccione funcion a utilizar:\n");
    printf("1.- execl\n");
    printf("2.- execlp\n");
    printf("3.- execv\n");
    printf("4.- execvp\n");
    printf("Opcion :");
    scanf("%d", &op);
    
    switch (op) {
        case 1:
            execl("/usr/bin/xterm","/usr/bin/xterm","-fn","9x15",NULL);
            break;
        case 2:
            execlp("xterm","xterm","-fn","9x15",NULL);
            break;
        case 3:
            execv("/usr/bin/xterm",argsv);
            break;
        case 4:
            execvp("xterm",argsvp);
            break;
        default: printf("Opcion no valida\n");
    }
    
    printf("Esto no se imprime!!!\n");
    return 1;
}
