#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

#define N 5

int main(){
    int j,i,status=0;
    int cont=0;
    for(i=0;i<N;i++){
        if(!fork()){
            break;
        }
    }

    sleep(8);

    if(i==N){
        for(i=0;i<N;i++){
            wait(&status);
            cont = cont + WEXITSTATUS(status);
        }
        printf("Desendientes = %d\n",cont);
    }else{
        if (j == N-1) {
          exit(1);
      }else{
        wait(&status);
        cont = cont + WEXITSTATUS(status);
      }
        exit(cont+1);
    }

    return 1;
}
