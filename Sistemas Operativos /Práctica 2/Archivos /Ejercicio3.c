#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int args, char *argv[]){
	int i, status=0;
	int cont=0;
	int j;
	int prof;
    int c = 0;
    printf("Ingresa el nivel de profundidad de tu arbol de procesos\n");
	scanf("%d", &prof);
	for(i = 0; i < prof; i++){ 
		for(j = 0; j < 2; j++){ 
			if(c < 2){
				if(fork()==0){
					c = 0; 
					break;
				}
			}
			c++;
		}
		if(c>=2){
			break;
		}
	}
	if(i == 0){
		for(j=0;j<2;j++){
			wait(&status);
			cont = cont + WEXITSTATUS(status);
		}
		printf("Soy el proceso %d: y tengo  %d descendientes\n",getpid(),cont);
	}else{
		if(i==prof){
			exit(1);
		}else{
			for(j=0;j<2;j++){
				wait(&status);
				cont = cont + WEXITSTATUS(status);
			}
			printf("Soy el proceso %d: y tengo  %d descendientes\n",getpid(),cont);
			exit(cont+1);
		}
	}
	return 0;
}
