#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int x = 1;

int main(){
    pid_t ret;
    FILE *apArch;
    int y = 2;
    
    if((apArch=fopen("salida.txt","w"))==NULL){
        printf("Error al abrir el archivo\n");
    }
       
       ret = fork();
       switch(ret){
           	case -1 : printf("No se pudo crear el proceso hijo.\n");
               break;
           	case 0 : x = x+1;
               y = y+1;
               fprintf(apArch, "Soy el proceso hijo pid: %d, pidPadre: %d, x=%d, y=%d\n",getpid(),getppid(),x,y);
               fclose(apArch);
               break;
           	default : x=x+2;
               y=y+2;
               fprintf(apArch, "Soy el proceso padre pid: %d, pidHijo: %d, x=%d, y=%d\n",getpid(),ret,x,y);
               fclose(apArch);
       }
       
       system("cat ./salida.txt");
       return 1;
}
