#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int varGlobal = 1000;
void ejecutaHilo(void* id);
#define N 6	
 
int main(){
	pthread_t h1,h2;
	int v1=5, v2=6;
	int *r1=NULL;
	int i,j;
 	pthread_t Hilos[N];

	for(i=0;i<N;i++){
		pthread_create(&Hilos[i],NULL,(void *)&ejecutaHilo,(void*)&i);
		sleep(1);
	}
	for(i=0;i<N;i++){
		pthread_join(Hilos[i],(void*)r1);
	}
	return 0;
}

void ejecutaHilo(void*  v){
	int v_anterior, v_siguiente;
	int *vh = (int  *) (v);
	
	if(*vh > 0 && *vh < N-1){
		v_anterior = *vh-1;
		v_siguiente = *vh+1;
	}else{
		if(*vh==0){
			v_anterior = N-1;
			v_siguiente = *vh+1;
		}else{
			v_anterior = *vh-1;
			v_siguiente = 0;
		}
	}
	printf("Hilo: %d  vecino anterior: %d  vecino siguiente: %d\n",*vh,v_anterior,v_siguiente);
	pthread_exit(vh);
}
