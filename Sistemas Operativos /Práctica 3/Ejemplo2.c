#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/call.h>

void ejecutaHilo(void* id);
 
int main(){
	pthread_t h1,h2;
	int id1=10, id2=20;
	pthread_create(&h1,NULL,(void *)&ejecutaHilo,(void*)&id1);
	printf("hilo1 (%u) creado \n", (unsigned int)h1);
	pthread_create(&h2,NULL,(void *)&ejecutaHilo,(void*)&id2);
	printf("hilo2 (%u) creado \n", (unsigned int)h2);

	pthread_join(h1,NULL);
	pthread_join(h2,NULL);
	return 0;
}

void ejecutaHilo(void* id){
	
	int *idh = (int  *) (id);
	int tid= syscall(SYS_gettid);
	printf("Hilo en ejecución con idPosix= %u, idLinux=%d, idPrograma= %d\n",(int)pthread_self(),tid,*idh);
	sleep(7);
	pthread_exit(NULL);
}
