#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void ejecutaHilo();

int main(){
	pthread_t h1,h2;
	pthread_create(&h1,NULL,(void *)&ejecutaHilo,NULL);
	pthread_create(&h2,NULL,(void *)&ejecutaHilo,NULL);

	pthread_join(h1,NULL);
	pthread_join(h2,NULL);
	return 0;
}

void ejecutaHilo(){
	printf("Hilo en ejecucion........\n");
	sleep(5);
	pthread_exit(NULL);
}
