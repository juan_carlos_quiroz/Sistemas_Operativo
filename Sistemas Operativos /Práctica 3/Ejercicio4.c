

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int varGlobal = 1000;
void ejecutaHilo(void* id);
#define N 6	
 
int main(){
	pthread_t h1,h2;
	int v1=5, v2=6;
	int *r1=NULL;
	int i,j;
 	pthread_t Hilos[N];

	for(i=0;i<N;i++){
		pthread_create(&Hilos[i],NULL,(void*)&ejecutaHilo,(void*)&i);
		sleep(1);
	}
	for(i=0;i<N;i++){
		pthread_join(Hilos[i],(void*)r1);
	}
	sleep(5);
	return 0;
}

void ejecutaHilo(void* v){
	
	int *vh = (int*) (v);
	//cada hilo escribe su valor de i en la variable global
	varGlobal=*vh;
	printf("Hilo: %d varGlobal=%d\n",*vh, varGlobal);
	pthread_exit(vh);
}


