#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define TAM 10
#define CON 8
#define PRO 5

int almacen[TAM];
sem_t espacios,elementos;
pthread_mutex_t c1, c2;
int prod[2];
int pos[2];

int produce(int p);
void productor(void *p);
void consumidor(void *p);

int main(){
	int i;
	pthread_t productores[PRO];
	pthread_t consumidores[CON];

	sem_init(&espacios,0,TAM);
	sem_init(&elementos,0,0);

	//Variables para Productores
	prod[0]=0;
	pos[0]=0;

	//Variables para Consumidores
	prod[1]=0;
	pos[1]=0;

	for(i=0;i<PRO;i++){
		pthread_create(&productores[i],NULL,(void*)&productor,(void*)&i);
	}

	for(i=0;i<CON;i++){
		pthread_create(&consumidores[i],NULL,(void*)&consumidor,(void*)&i);
	}

	for(i=0;i<PRO;i++){
		pthread_join(productores[i],NULL);
	}


	for(i=0;i<CON;i++){
		pthread_join(consumidores[i],NULL);
	}

	return 0;
}

void productor(void *p){
	

	while(1){
		sem_wait(&espacios);
			pthread_mutex_lock(&c1);
				prod[0]++;
				almacen[pos[0]] = prod[0];
				printf("Produciendo: %d en almacen[ %d] \n", prod[0],pos[0]);
				pos[0] = (pos[0]+1)%TAM;
			pthread_mutex_unlock(&c1);
			sleep(2);
		sem_post(&elementos);
	}
}

void consumidor(void *p){
	while(1){
		sem_wait(&elementos);
			pthread_mutex_lock(&c1);
				prod[1]=almacen[pos[1]];
				printf("\tConsumiendo: %d de almacen[ %d] \n", prod[1],pos[1]);
				pos[1]=(pos[1]+1) %TAM;
			pthread_mutex_unlock(&c1);
			sleep(2);
		sem_post(&espacios);
		
	}
}
