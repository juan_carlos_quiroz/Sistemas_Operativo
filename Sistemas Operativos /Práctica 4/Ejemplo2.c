#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define TAM 10

int almacen[TAM];
sem_t espacios,elementos;

void productor(void *p);
void consumidor(void *p);

int main(){
	pthread_t hilo1,hilo2;
	void *res;

	sem_init(&espacios,0,TAM);
	sem_init(&elementos,0,0);

	pthread_create(&hilo2,NULL,(void*)&consumidor,(void *)NULL);
	pthread_create(&hilo1,NULL,(void*)&productor,(void *)NULL);

	pthread_join(hilo1,&res);
	pthread_join(hilo2,&res);

	return 0;
}

void productor(void *p){
	int producto = 0;
	int pos=0;

	while(1){
		producto++;
		sem_wait(&espacios);
			printf("Produciendo: %d en almacen[ %d] \n", producto,pos);
			almacen[pos]=producto;
			pos=(pos+1) %TAM;
		sem_post(&elementos);
		sleep(1);
	}
}

void consumidor(void *p){
	int producto=0;
	int pos=0;
 
	while(1){
		sem_wait(&elementos);
			producto=almacen[pos];
			printf("\tConsumiendo: %d de almacen[ %d] \n", producto,pos);
			pos=(pos+1) %TAM;
		sem_post(&espacios);
		sleep(2);
	}
}