#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

#define LEER 0
#define ESCRIBIR 1
#define N 20

void generaPares(int tuberia, int t1, int t2);
void generaImpares(int tuberia, int t1, int t2);
void consumirNumeros(int tuberia);

int main(){
	int tuberia[2], t1[2], t2[2];
	int pid1=getpid(), pid2, pid3;
	int i;
	pipe(tuberia);
	pipe(t1);
	pipe(t2);

	switch(pid2=fork()){
		case 0:
			generaPares(tuberia[ESCRIBIR], t1[ESCRIBIR], t2[LEER]);
			
			exit(1);
		default:  	switch(pid3=fork()){
						case 0:
							consumirNumeros(tuberia[LEER]);
							exit(1);
						default:
							generaImpares(tuberia[ESCRIBIR], t1[LEER], t2[ESCRIBIR]);
					}
	}
	
	wait(NULL);
	wait(NULL);
	return 0;
}

void generaPares(int tuberia, int t1, int t2){
	char testigo='t';
	int msje=0;

	while(msje<N-1){
		write(tuberia,&msje,sizeof(int));
		write(t1,&msje,sizeof(int));
		read(t2, &msje, sizeof(int));
		msje++;
	}
}

void generaImpares(int tuberia, int t1, int t2){
	char testigo='t';
	int msje;

	while(msje<N-1){
		read(t1, &msje, sizeof(int));
		msje++;
		write(tuberia,&msje,sizeof(int));
		write(t2,&msje,sizeof(int));
	}
}

void consumirNumeros(int tuberia){
	int i, msje;
	for(i=0;i<N;i++){
		read(tuberia, &msje, sizeof(int));
		printf(" %d\n", msje);
		sleep(1);
	}
}