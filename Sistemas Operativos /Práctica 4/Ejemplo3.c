#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

#define N 10

int main(){
	int fildes[2];
	int producto=0;
	int i;

	pipe(fildes);

	if (!fork()){
		for(i=0;i<N;i++){
			producto++;
			write(fildes[1],&producto,sizeof(int));
		}
		exit(1);
	}else{
		for(i=0;i<N;i++){
			producto++;
			read(fildes[0],&producto,sizeof(int));
			printf("Consume %d\n", producto);
			sleep(1);
		}
		wait(NULL);
	}
	
	return 0;
} 