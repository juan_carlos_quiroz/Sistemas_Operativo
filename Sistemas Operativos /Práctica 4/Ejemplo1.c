#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

int almacen=-5;
pthread_mutex_t c1,c2;

void productor(void *p);
void consumidor(void *p);

int main(){
	pthread_t hilo1,hilo2;
	void *res;

	pthread_mutex_lock(&c2);

	pthread_create(&hilo2,NULL,(void*)&consumidor,(void *)NULL);
	pthread_create(&hilo1,NULL,(void*)&productor,(void *)NULL);

	pthread_join(hilo1,&res);
	pthread_join(hilo2,&res);

}

void productor(void *p){
	int producto = 0;

	while(1){
		producto++;
		pthread_mutex_lock(&c1);
			almacen=producto;
		pthread_mutex_unlock(&c2);
		sleep(1);
	}
}

void consumidor(void *p){
	while(1){
		pthread_mutex_lock(&c2);
			printf("Consumiendo %d \n", almacen);
		pthread_mutex_unlock(&c1);
	}
}